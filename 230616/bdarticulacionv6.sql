-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-06-2016 a las 01:00:04
-- Versión del servidor: 10.1.13-MariaDB
-- Versión de PHP: 5.5.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdarticulacionv6`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acta`
--

CREATE TABLE `acta` (
  `IdActa` int(11) NOT NULL,
  `Fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fkIdCiudad` int(11) NOT NULL,
  `HoraInicio` datetime DEFAULT NULL,
  `HoraFinalizacion` datetime DEFAULT NULL,
  `fkIdEntidad` int(11) NOT NULL,
  `Temas` varchar(100) DEFAULT NULL,
  `Objetivo` varchar(100) DEFAULT NULL,
  `Desarrollo` varchar(100) DEFAULT NULL,
  `Conclusiones` varchar(100) DEFAULT NULL,
  `ArchivoAdjuntado` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `acta`
--

INSERT INTO `acta` (`IdActa`, `Fecha`, `fkIdCiudad`, `HoraInicio`, `HoraFinalizacion`, `fkIdEntidad`, `Temas`, `Objetivo`, `Desarrollo`, `Conclusiones`, `ArchivoAdjuntado`) VALUES
(1, '2016-06-15 18:30:30', 1, '2016-06-15 14:33:36', '2016-06-15 16:33:35', 2, 'Revision', 'Observar los ambientes y encargados', NULL, 'El ambiente es apto para los aprendices ', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividad`
--

CREATE TABLE `actividad` (
  `IdActividad` int(11) NOT NULL,
  `fkIdTipoActividad` int(11) NOT NULL,
  `Descripción` varchar(90) DEFAULT NULL,
  `CargoCarrera` varchar(90) DEFAULT NULL,
  `Entidad` varchar(90) DEFAULT NULL,
  `TiempoTranscurrido` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `actividad`
--

INSERT INTO `actividad` (`IdActividad`, `fkIdTipoActividad`, `Descripción`, `CargoCarrera`, `Entidad`, `TiempoTranscurrido`) VALUES
(1, 1, 'Estoy trabajando en empresa de software online', 'jefe de desarrollo', 'CDI', '2016-04-05 14:23:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agenda`
--

CREATE TABLE `agenda` (
  `IdAgenda` int(11) NOT NULL,
  `FechaProgramada` date DEFAULT NULL,
  `fkIdPersona` int(11) NOT NULL,
  `fkIdEstado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `agenda`
--

INSERT INTO `agenda` (`IdAgenda`, `FechaProgramada`, `fkIdPersona`, `fkIdEstado`) VALUES
(1, '2016-06-16', 5, 1),
(2, '2016-06-16', 5, 1),
(3, '2016-06-28', 5, 1),
(4, '2016-06-22', 1, 1),
(5, '2016-06-25', 5, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aprendiz`
--

CREATE TABLE `aprendiz` (
  `IdAprendiz` int(11) NOT NULL,
  `fkIdTipoDocumento` int(11) NOT NULL,
  `Documento` varchar(90) DEFAULT NULL,
  `Nombre` varchar(100) DEFAULT NULL,
  `Apellido` varchar(100) DEFAULT NULL,
  `FechaNacimiento` date DEFAULT NULL,
  `Telefono` varchar(90) DEFAULT NULL,
  `Correo` varchar(90) DEFAULT NULL,
  `Contraseña` varchar(89) DEFAULT NULL,
  `fkIdFicha` int(11) NOT NULL,
  `fkIdEntidad` int(11) NOT NULL,
  `fkIdCargo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `aprendiz`
--

INSERT INTO `aprendiz` (`IdAprendiz`, `fkIdTipoDocumento`, `Documento`, `Nombre`, `Apellido`, `FechaNacimiento`, `Telefono`, `Correo`, `Contraseña`, `fkIdFicha`, `fkIdEntidad`, `fkIdCargo`) VALUES
(2, 2, '165546456', 'juan', 'Perez', '1999-01-23', '5102100', 'jperez@gmail.com', 'peresss', 1, 2, 8),
(3, 1, '135151531', 'Wynter', 'Heath', '0000-00-00', '4540000', 'Cum@eu.co.uk', '1652110810699', 1, 1, 8),
(4, 2, '6544654', 'Rebecca', 'George', '0000-00-00', '3637099', 'Integer.tincidunt@blanditcongueIn.net', '1624040201999', 1, 2, 8),
(5, 2, '531515144', 'Dai', 'Noble', '0000-00-00', '7166496', 'quam.dignissim.pharetra@penatibus.net', '1607012108999', 1, 2, 8),
(6, 2, '468416511', 'Tiger', 'Kane', '0000-00-00', '9226813', 'aliquet@Namac.org', '1669111701999', 1, 2, 8),
(7, 1, '646636', 'Edan', 'Allison', '0000-00-00', '9276097', 'semper@arcuCurabiturut.edu', '1691011264099', 2, 2, 8),
(8, 2, '351153151', 'Wylie', 'Daniels', '0000-00-00', '6830505', 'sem@tristiquesenectuset.com', '1671060482099', 2, 2, 8),
(9, 1, '4684616516', 'Erasmus', 'Valdez', '0000-00-00', '1684936', 'nonummy.ultricies@dolor.net', '1608080611899', 2, 2, 8),
(10, 2, '884811', 'Anthony', 'Beasley', '0000-00-00', '2656468', 'quam.elementum.at@tellusimperdietnon.co.uk', '1664102993399', 2, 2, 8),
(11, 1, '135151531', 'Melyssa', 'Guerrero', '0000-00-00', '7786961', 'sociis.natoque.penatibus@dapibusid.org', '1633102478299', 3, 1, 8),
(12, 1, '531515144', 'Morgan', 'Jordan', '0000-00-00', '5281240', 'Nam.interdum@Sedid.net', '1692112570299', 3, 1, 8),
(13, 1, '646636', 'Wyoming', 'Thomas', '0000-00-00', '1017198', 'posuere@ullamcorpervelitin.ca', '1698072064999', 3, 2, 8),
(14, 1, '351153151', 'Lee', 'Livingston', '0000-00-00', '7633027', 'erat.Etiam.vestibulum@faucibusorciluctus.net', '1682042588799', 3, 1, 8),
(15, 2, '468416511', 'Brenda', 'Shaffer', '0000-00-00', '1890185', 'Ut.tincidunt.orci@Integerin.net', '1666041547499', 3, 1, 8),
(16, 1, '841113', 'Carl', 'Hooper', '0000-00-00', '3989695', 'natoque.penatibus@gravidamauris.edu', '1665071793699', 4, 2, 8),
(17, 1, '6544654', 'Yuri', 'Salinas', '0000-00-00', '8172976', 'ut.molestie.in@enimmi.co.uk', '1622060954599', 4, 1, 8),
(18, 1, '51531531531', 'Colorado', 'Mathews', '0000-00-00', '8727709', 'aliquet.Phasellus.fermentum@Suspendissealiquetsem.co.uk', '1637120999899', 4, 2, 8),
(19, 1, '', 'Malcolm', 'Tyler', '0000-00-00', '7662544', 'in.felis.Nulla@Vestibulumante.com', '1606110174799', 4, 1, 8),
(20, 1, '884811', 'Victoria', 'Oconnor', '0000-00-00', '7463780', 'amet.dapibus.id@ProinvelitSed.ca', '1687042216799', 4, 1, 8),
(21, 2, '', 'Nora', 'Jordan', '0000-00-00', '8430064', 'lectus.justo.eu@turpisegestas.co.uk', '1687072307099', 5, 2, 8),
(22, 1, '51531531531', 'Dustin', 'Cook', '0000-00-00', '1042708', 'rhoncus.Donec.est@nequepellentesque.org', '1676101187799', 5, 1, 8),
(23, 1, '54354364', 'Stacy', 'Hall', '0000-00-00', '2167032', 'Phasellus.nulla.Integer@gravida.ca', '1690102824299', 5, 2, 8),
(24, 1, '841113', 'Alexandra', 'Arnold', '0000-00-00', '4719325', 'non.cursus.non@libero.ca', '1645071401499', 5, 1, 8),
(25, 2, '646636', 'Fritz', 'Cabrera', '0000-00-00', '0203572', 'gravida.sagittis@dolortempusnon.ca', '1679032098799', 5, 2, 8),
(26, 2, '4684616516', 'Finn', 'Macdonald', '0000-00-00', '4845748', 'Lorem.ipsum.dolor@libero.net', '1636062785499', 6, 7, 8),
(27, 2, '841113', 'Kimberley', 'Hogan', '0000-00-00', '9177294', 'sociis@metus.ca', '1657032799299', 6, 7, 8),
(28, 2, '51531531531', 'Yetta', 'Britt', '0000-00-00', '3309849', 'ante.ipsum@acorciUt.edu', '1606040929399', 6, 7, 8),
(29, 2, '468416511', 'Nolan', 'Mills', '0000-00-00', '7770940', 'quam.a.felis@Curabiturmassa.com', '1602043089099', 6, 7, 8),
(30, 2, '6544654', 'Merrill', 'Nelson', '0000-00-00', '1659028', 'sit.amet@aliquam.co.uk', '1666070333499', 6, 7, 8),
(31, 1, '884811', 'Christopher', 'Montgomery', '0000-00-00', '6037297', 'libero.lacus@sagittisfelisDonec.net', '1689022313299', 7, 7, 8),
(32, 1, '531515144', 'Sierra', 'Greer', '0000-00-00', '9305601', 'pede@ac.co.uk', '1651032134399', 7, 7, 8),
(33, 2, '841113', 'Lisandra', 'Norman', '0000-00-00', '2596734', 'ipsum.non.arcu@ametmetusAliquam.net', '1601061713399', 7, 7, 8),
(34, 2, '531515144', 'Jesse', 'Berry', '0000-00-00', '5689865', 'turpis.egestas.Fusce@elitCurabitur.co.uk', '1688042903099', 7, 7, 8),
(35, 1, '468416511', 'Isadora', 'Vega', '0000-00-00', '8266461', 'aliquet.vel@adipiscinglacusUt.co.uk', '1600111793699', 7, 7, 8),
(36, 2, '884811', 'Keegan', 'Prince', '0000-00-00', '5923223', 'lobortis.Class@viverra.co.uk', '1616080785099', 8, 7, 8),
(37, 1, '51531531531', 'Zia', 'Mcmahon', '0000-00-00', '0019684', 'luctus@purusac.net', '1628021217999', 8, 7, 8),
(38, 2, '468416511', 'Germaine', 'Curtis', '0000-00-00', '2206222', 'tellus.non.magna@fermentumvelmauris.net', '1685020392199', 8, 7, 8),
(39, 1, '351153151', 'Thaddeus', 'Wolfe', '0000-00-00', '4240402', 'Aliquam.fringilla.cursus@CraspellentesqueSed.com', '1679072712499', 8, 7, 8),
(40, 2, '351153151', 'Alma', 'Mcintyre', '0000-00-00', '8132653', 'odio.Phasellus.at@Suspendissecommodo.org', '1657031843099', 8, 7, 8),
(41, 2, '51531531531', 'Harper', 'Bullock', '0000-00-00', '2180931', 'sapien@iaculisodioNam.net', '1640091037399', 9, 7, 8),
(42, 1, '884811', 'Bianca', 'Rogers', '0000-00-00', '9569895', 'Duis@eu.org', '1610022026599', 9, 7, 8),
(43, 1, '351153151', 'Laura', 'Gentry', '0000-00-00', '3073503', 'Aliquam.vulputate@vulputatemauris.com', '1602062775599', 9, 7, 8),
(44, 1, '841113', 'Nolan', 'Carlson', '0000-00-00', '7288191', 'elit.Aliquam@Vivamuseuismod.co.uk', '1622031532999', 9, 7, 8),
(45, 2, '646636', 'Idona', 'Newton', '0000-00-00', '3530900', 'imperdiet@Fuscemi.edu', '1686121882499', 9, 7, 8),
(46, 1, '531515144', 'Ray', 'Howard', '0000-00-00', '1964979', 'Cras@quisaccumsanconvallis.edu', '1639010705399', 1, 4, 8),
(47, 2, '135151531', 'Honorato', 'Travis', '0000-00-00', '3471499', 'nibh.sit.amet@auctor.org', '1664011694099', 1, 4, 8),
(48, 1, '51531531531', 'Audrey', 'Dyer', '0000-00-00', '5557468', 'nulla.at@Maurisvestibulum.ca', '1695031183199', 1, 4, 8),
(49, 2, '6544654', 'Brianna', 'Hopper', '0000-00-00', '0634475', 'elementum.lorem.ut@velitQuisque.ca', '1613082451499', 1, 4, 8),
(50, 1, '841113', 'Troy', 'Patrick', '0000-00-00', '4468001', 'odio.a@sedturpisnec.ca', '1641012764699', 1, 4, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `area`
--

CREATE TABLE `area` (
  `IdArea` int(11) NOT NULL,
  `NombreArea` varchar(90) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `area`
--

INSERT INTO `area` (`IdArea`, `NombreArea`) VALUES
(1, 'Mercados'),
(2, 'Logistica'),
(3, 'Tecnologias de la informacion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignacionaprendizbitacora`
--

CREATE TABLE `asignacionaprendizbitacora` (
  `IdAsignacionAprendizBitacora` int(11) NOT NULL,
  `fkIdBitacora` int(11) NOT NULL,
  `fkIdAprendiz` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `asignacionaprendizbitacora`
--

INSERT INTO `asignacionaprendizbitacora` (`IdAsignacionAprendizBitacora`, `fkIdBitacora`, `fkIdAprendiz`) VALUES
(1, 2, 21);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignacionaprendizegresado`
--

CREATE TABLE `asignacionaprendizegresado` (
  `IdAsignacionAprendizEgresado` int(11) NOT NULL,
  `fkIdAPrendiz` int(11) NOT NULL,
  `fkIdActividad` int(11) NOT NULL,
  `fkIdEstado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `asignacionaprendizegresado`
--

INSERT INTO `asignacionaprendizegresado` (`IdAsignacionAprendizEgresado`, `fkIdAPrendiz`, `fkIdActividad`, `fkIdEstado`) VALUES
(2, 22, 1, 16),
(3, 25, 1, 16);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignacioncargo`
--

CREATE TABLE `asignacioncargo` (
  `IdAsignacionCargo` int(11) NOT NULL,
  `fkIdPersona` int(11) NOT NULL,
  `fkIdCargo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `asignacioncargo`
--

INSERT INTO `asignacioncargo` (`IdAsignacionCargo`, `fkIdPersona`, `fkIdCargo`) VALUES
(1, 1, 2),
(2, 1, 3),
(3, 6, 6),
(4, 6, 5),
(5, 5, 3),
(6, 4, 7),
(7, 3, 7),
(8, 2, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignacioncompromisos`
--

CREATE TABLE `asignacioncompromisos` (
  `IdAsignacionCompromisos` int(11) NOT NULL,
  `fkIdActa` int(11) NOT NULL,
  `fkIdCompromisos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `asignacioncompromisos`
--

INSERT INTO `asignacioncompromisos` (`IdAsignacionCompromisos`, `fkIdActa`, `fkIdCompromisos`) VALUES
(1, 1, 1),
(2, 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignacionentidadagenda`
--

CREATE TABLE `asignacionentidadagenda` (
  `IdAsignacionEntidadAgenda` int(11) NOT NULL,
  `fkIdAgenda` int(11) NOT NULL,
  `fkIdEntidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `asignacionentidadagenda`
--

INSERT INTO `asignacionentidadagenda` (`IdAsignacionEntidadAgenda`, `fkIdAgenda`, `fkIdEntidad`) VALUES
(1, 1, 2),
(2, 4, 2),
(3, 1, 7),
(4, 4, 7),
(5, 2, 5),
(6, 4, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignacionestadoaprendiz`
--

CREATE TABLE `asignacionestadoaprendiz` (
  `IdAsignacionEstadoAprendiz` int(11) NOT NULL,
  `fkIdEstado` int(11) NOT NULL,
  `fkIdAprendiz` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `asignacionestadoaprendiz`
--

INSERT INTO `asignacionestadoaprendiz` (`IdAsignacionEstadoAprendiz`, `fkIdEstado`, `fkIdAprendiz`) VALUES
(1, 12, 8),
(2, 12, 2),
(3, 12, 5),
(4, 12, 3),
(5, 12, 4),
(7, 12, 6),
(8, 13, 10),
(9, 13, 11),
(10, 13, 12),
(11, 13, 13),
(12, 13, 14),
(13, 13, 15),
(14, 13, 16),
(15, 13, 17),
(16, 13, 18),
(17, 15, 21),
(18, 15, 22),
(19, 15, 23),
(20, 15, 24),
(21, 15, 25),
(22, 15, 26),
(23, 14, 2),
(24, 14, 3),
(25, 14, 4),
(26, 14, 5),
(27, 14, 6),
(28, 14, 7),
(29, 14, 8),
(30, 14, 9),
(31, 14, 10),
(32, 14, 11),
(33, 14, 12),
(34, 14, 13),
(35, 14, 14),
(36, 14, 15),
(37, 14, 16),
(38, 14, 17),
(39, 14, 18),
(40, 14, 19),
(41, 14, 20);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignacionpersonaficha`
--

CREATE TABLE `asignacionpersonaficha` (
  `IdAsignacionPersonaFicha` int(11) NOT NULL,
  `fkIdPersona` int(11) NOT NULL,
  `fkIdFicha` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `asignacionpersonaficha`
--

INSERT INTO `asignacionpersonaficha` (`IdAsignacionPersonaFicha`, `fkIdPersona`, `fkIdFicha`) VALUES
(1, 6, 1),
(2, 5, 1),
(3, 1, 2),
(4, 1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignacionpersonaparticipanteacta`
--

CREATE TABLE `asignacionpersonaparticipanteacta` (
  `IdAsignacionPersonaParticipanteActa` int(11) NOT NULL,
  `fkIdTipoParticipante` int(11) NOT NULL,
  `fkIdPersona` int(11) NOT NULL,
  `fkIdActa` int(11) NOT NULL,
  `Firma` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `asignacionpersonaparticipanteacta`
--

INSERT INTO `asignacionpersonaparticipanteacta` (`IdAsignacionPersonaParticipanteActa`, `fkIdTipoParticipante`, `fkIdPersona`, `fkIdActa`, `Firma`) VALUES
(1, 1, 6, 1, NULL),
(2, 2, 5, 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignacionrequisitosaprendiz`
--

CREATE TABLE `asignacionrequisitosaprendiz` (
  `IdAsignacionRequisitosAprendiz` int(11) NOT NULL,
  `fkIdAprendiz` int(11) NOT NULL,
  `fkIdRequisitoCertificacion` int(11) NOT NULL,
  `fkIdEstado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `asignacionrequisitosaprendiz`
--

INSERT INTO `asignacionrequisitosaprendiz` (`IdAsignacionRequisitosAprendiz`, `fkIdAprendiz`, `fkIdRequisitoCertificacion`, `fkIdEstado`) VALUES
(1, 2, 1, 11),
(2, 2, 2, 11),
(3, 2, 3, 11),
(4, 2, 4, 9),
(5, 2, 5, 9),
(6, 3, 1, 11),
(7, 3, 2, 11),
(8, 3, 3, 11),
(9, 3, 4, 11),
(10, 3, 5, 9),
(11, 25, 1, 9),
(12, 25, 2, 9),
(13, 25, 3, 9),
(14, 25, 4, 9),
(15, 25, 5, 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignaciontarea`
--

CREATE TABLE `asignaciontarea` (
  `IdAsignacionTarea` int(11) NOT NULL,
  `fkIdTarea` int(11) NOT NULL,
  `fkIdBitacora` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `asignaciontarea`
--

INSERT INTO `asignaciontarea` (`IdAsignacionTarea`, `fkIdTarea`, `fkIdBitacora`) VALUES
(1, 1, 2),
(2, 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bitacora`
--

CREATE TABLE `bitacora` (
  `IdBitacora` int(11) NOT NULL,
  `Fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fkIdCiudad` int(11) NOT NULL,
  `fkIdEntidad` int(11) NOT NULL,
  `fkIdPersona` int(11) NOT NULL,
  `Firma` varchar(30) DEFAULT NULL,
  `ArchivoAdjunto` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `bitacora`
--

INSERT INTO `bitacora` (`IdBitacora`, `Fecha`, `fkIdCiudad`, `fkIdEntidad`, `fkIdPersona`, `Firma`, `ArchivoAdjunto`) VALUES
(2, '2016-05-17 05:00:00', 1, 3, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo`
--

CREATE TABLE `cargo` (
  `IdCargo` int(11) NOT NULL,
  `NombreCargo` varchar(90) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cargo`
--

INSERT INTO `cargo` (`IdCargo`, `NombreCargo`) VALUES
(1, 'Lider Articulacion'),
(2, 'Lider Area'),
(3, 'Instructor'),
(4, 'Instructor Seguimiento'),
(5, 'Docente'),
(6, 'Cordinador'),
(7, 'JefeInmediato'),
(8, 'Aprendiz');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `IdCiudad` int(11) NOT NULL,
  `NombreCiudad` varchar(90) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`IdCiudad`, `NombreCiudad`) VALUES
(1, 'Bogota'),
(2, 'Cali'),
(3, 'Medellin'),
(4, 'Meta'),
(5, 'Cartagena');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compromisos`
--

CREATE TABLE `compromisos` (
  `IdCompromisos` int(11) NOT NULL,
  `Actividad` varchar(100) DEFAULT NULL,
  `fkIdPersona` int(11) NOT NULL,
  `Fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `compromisos`
--

INSERT INTO `compromisos` (`IdCompromisos`, `Actividad`, `fkIdPersona`, `Fecha`) VALUES
(1, 'Revisar Ambientes', 6, '2016-05-17'),
(2, 'Revisar Equipos', 6, '2016-06-14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entidad`
--

CREATE TABLE `entidad` (
  `IdEntidad` int(11) NOT NULL,
  `fkIdTipoEntidad` int(11) NOT NULL,
  `Nit` int(11) DEFAULT NULL,
  `Nombre` varchar(90) DEFAULT NULL,
  `Telefono` varchar(90) DEFAULT NULL,
  `Direccion` varchar(90) DEFAULT NULL,
  `fkIdCiudad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `entidad`
--

INSERT INTO `entidad` (`IdEntidad`, `fkIdTipoEntidad`, `Nit`, `Nombre`, `Telefono`, `Direccion`, `fkIdCiudad`) VALUES
(1, 1, NULL, 'Servicio Nacional de Aprendizaje SENA', '7053654', 'calle 52 ', 1),
(2, 1, NULL, 'Colegio Aldemar Rojar Plazas', '654198487', 'calle falsa 5198', 1),
(3, 2, 2147483647, 'Banco Bogota', '4532424', 'calle falsa 56151', 2),
(4, 2, 85187187, 'Banco BBVA', '7452543285', 'calle false 9458', 3),
(5, 2, 518718787, 'Empresa de Informatica', '45354374', 'calle falsa 7487814', 4),
(7, 1, NULL, 'Enrique Olaya Herrera', '654198487', 'calle falsa 5198', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `IdEstado` int(11) NOT NULL,
  `NombreEstado` varchar(90) DEFAULT NULL,
  `fkIdTipoEstado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`IdEstado`, `NombreEstado`, `fkIdTipoEstado`) VALUES
(1, 'Activo', 1),
(2, 'Inactivo', 2),
(9, 'Aprobado', 3),
(10, 'Reprobado', 3),
(11, 'Pendiente', 3),
(12, 'Tecnico', 2),
(13, 'Tecnologo', 2),
(14, 'Lectiva', 2),
(15, 'Productiva', 2),
(16, 'CasoExito', 5),
(17, 'Sin Respuesta', 4),
(18, 'Respondido', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ficha`
--

CREATE TABLE `ficha` (
  `IdFicha` int(11) NOT NULL,
  `NumeroFicha` varchar(99) DEFAULT NULL,
  `fkIdPrograma` int(11) NOT NULL,
  `fkIdEstado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ficha`
--

INSERT INTO `ficha` (`IdFicha`, `NumeroFicha`, `fkIdPrograma`, `fkIdEstado`) VALUES
(1, '1124806', 3, 1),
(2, '518718', 2, 1),
(3, '8981881', 1, 1),
(4, '5418181', 4, 1),
(5, '5187178711', 5, 1),
(6, '11568925', 6, 1),
(7, '56895212', 7, 1),
(8, '5589652', 8, 1),
(9, '5856541', 9, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `IdPersona` int(11) NOT NULL,
  `fkIdTipoDocumento` int(11) NOT NULL,
  `Documento` varchar(50) NOT NULL,
  `Nombre` varchar(90) DEFAULT NULL,
  `Apellido` varchar(90) DEFAULT NULL,
  `FechaNacimiento` varchar(50) DEFAULT NULL,
  `Telefono` bigint(50) DEFAULT NULL,
  `Correo` varchar(90) DEFAULT NULL,
  `Contraseña` varchar(30) DEFAULT NULL,
  `fkIdEntidad` int(11) NOT NULL,
  `fkIdEstado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`IdPersona`, `fkIdTipoDocumento`, `Documento`, `Nombre`, `Apellido`, `FechaNacimiento`, `Telefono`, `Correo`, `Contraseña`, `fkIdEntidad`, `fkIdEstado`) VALUES
(1, 1, '783285434', 'Diego Alejandro', 'Baoda', '31/19/1998', 85278525, 'Diego@hotmail.com', '134679258', 1, 1),
(2, 2, '5619819', 'Alejandro', 'Munevar', '18/16/1994', 18718, 'Alejandro@ergaga.com', '4518979487', 2, 1),
(3, 3, '451515515', 'Luiz Eduardo', 'Pertuz', '17/19/1995', 5411884, 'Angelica@frhsrh.com', '541874187', 4, 1),
(4, 4, '51874874', 'Roberto', 'Casas', '18/18/1991', 894198418, 'Roberto@dfhbsth.com', '541541dag', 5, 1),
(5, 5, '4184181', 'Paola ', 'Cardenas', '18/17/1885', 874871, 'Paola@adgfafg.com', '487874sfdg', 1, 1),
(6, 1, '8154455525', 'Hector ', 'Mora', '24/10/1987', 5102102, 'hector@gmail.com', '123', 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pqr`
--

CREATE TABLE `pqr` (
  `IdPQR` int(11) NOT NULL,
  `Fecha` date DEFAULT NULL,
  `fkIdAprendiz` int(11) NOT NULL,
  `fkIdTipoPQR` int(11) NOT NULL,
  `Asunto` varchar(90) DEFAULT NULL,
  `fkIdArea` int(11) NOT NULL,
  `Descripcion` varchar(90) DEFAULT NULL,
  `fkIdEstado` int(11) NOT NULL,
  `fkIdPersona` int(11) DEFAULT NULL,
  `Respuesta` varchar(90) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pqr`
--

INSERT INTO `pqr` (`IdPQR`, `Fecha`, `fkIdAprendiz`, `fkIdTipoPQR`, `Asunto`, `fkIdArea`, `Descripcion`, `fkIdEstado`, `fkIdPersona`, `Respuesta`) VALUES
(1, '2016-06-10', 4, 1, 'Dificultades', 1, 'No queda tiempo para nada', 15, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programa`
--

CREATE TABLE `programa` (
  `IdPrograma` int(11) NOT NULL,
  `Nombre` varchar(90) DEFAULT NULL,
  `fkIdArea` int(11) NOT NULL,
  `fkIdEstado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `programa`
--

INSERT INTO `programa` (`IdPrograma`, `Nombre`, `fkIdArea`, `fkIdEstado`) VALUES
(1, 'Programacion de software', 3, 1),
(2, 'Mantenimiento de Computadores', 3, 1),
(3, 'ADSI', 3, 1),
(4, 'Coordinacion Logistica', 2, 1),
(5, 'Control de Multitudes', 2, 1),
(6, 'Logistica de Bodegas', 2, 1),
(7, 'Cordinacion de Mercados', 1, 1),
(8, 'Venta de Productos', 1, 1),
(9, 'Ventas', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `requisitocertificacion`
--

CREATE TABLE `requisitocertificacion` (
  `IdRequisitoCertificacion` int(11) NOT NULL,
  `Requisito` varchar(100) DEFAULT NULL,
  `fkIdEstado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `requisitocertificacion`
--

INSERT INTO `requisitocertificacion` (`IdRequisitoCertificacion`, `Requisito`, `fkIdEstado`) VALUES
(1, 'Aprobar Proyecto', 1),
(2, 'Aprobar Asignaturas', 1),
(3, 'Aprobar Tecnico', 1),
(4, 'Aprobar Tecnologo', 1),
(5, 'Aprobar Nivel Ingles', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarea`
--

CREATE TABLE `tarea` (
  `IdTarea` int(11) NOT NULL,
  `Observacion` varchar(90) DEFAULT NULL,
  `Actividad` varchar(90) DEFAULT NULL,
  `Fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tarea`
--

INSERT INTO `tarea` (`IdTarea`, `Observacion`, `Actividad`, `Fecha`) VALUES
(1, '', 'Bases de datos', '2015-05-21'),
(2, '', 'Maquetacion', '2015-06-16'),
(3, '', 'Java', '2015-05-21'),
(4, '', 'Php', '2016-06-16'),
(5, '', 'C++', '2016-05-21'),
(6, '', 'Diseños web', '2016-06-16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoactividad`
--

CREATE TABLE `tipoactividad` (
  `IdTipoActividad` int(11) NOT NULL,
  `NombreTipo` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipoactividad`
--

INSERT INTO `tipoactividad` (`IdTipoActividad`, `NombreTipo`) VALUES
(1, 'laboral'),
(2, 'academica');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipodocumento`
--

CREATE TABLE `tipodocumento` (
  `IdTipoDocumento` int(11) NOT NULL,
  `NombreTipoDocumento` varchar(90) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipodocumento`
--

INSERT INTO `tipodocumento` (`IdTipoDocumento`, `NombreTipoDocumento`) VALUES
(1, 'CC'),
(2, 'TI'),
(3, 'CI'),
(4, 'CE'),
(5, 'DNI'),
(6, 'ID');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoentidad`
--

CREATE TABLE `tipoentidad` (
  `IdTipoEntidad` int(11) NOT NULL,
  `NombreTipoEntidad` varchar(90) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipoentidad`
--

INSERT INTO `tipoentidad` (`IdTipoEntidad`, `NombreTipoEntidad`) VALUES
(1, 'Institucion'),
(2, 'Empresa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoestado`
--

CREATE TABLE `tipoestado` (
  `IdTipoEstado` int(11) NOT NULL,
  `NombreTipoEstado` varchar(90) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipoestado`
--

INSERT INTO `tipoestado` (`IdTipoEstado`, `NombreTipoEstado`) VALUES
(1, 'Estado'),
(2, 'EtapaFormacion'),
(3, 'Requisitos'),
(4, 'Pqr'),
(5, 'Caso');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoparticipante`
--

CREATE TABLE `tipoparticipante` (
  `IdTipoParticipante` int(11) NOT NULL,
  `NombreTipoParticipante` varchar(90) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipoparticipante`
--

INSERT INTO `tipoparticipante` (`IdTipoParticipante`, `NombreTipoParticipante`) VALUES
(1, 'Asistente'),
(2, 'Invitado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipopqr`
--

CREATE TABLE `tipopqr` (
  `IdTipoPQR` int(11) NOT NULL,
  `Nombre` varchar(90) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipopqr`
--

INSERT INTO `tipopqr` (`IdTipoPQR`, `Nombre`) VALUES
(1, 'Peticion'),
(2, 'Queja'),
(3, 'Reclamo');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `acta`
--
ALTER TABLE `acta`
  ADD PRIMARY KEY (`IdActa`),
  ADD KEY `ActaEntidad` (`fkIdEntidad`),
  ADD KEY `CiudadActa` (`fkIdCiudad`);

--
-- Indices de la tabla `actividad`
--
ALTER TABLE `actividad`
  ADD PRIMARY KEY (`IdActividad`),
  ADD KEY `ActividadTipo` (`fkIdTipoActividad`);

--
-- Indices de la tabla `agenda`
--
ALTER TABLE `agenda`
  ADD PRIMARY KEY (`IdAgenda`),
  ADD KEY `AgendaPersona` (`fkIdPersona`),
  ADD KEY `EstadoAgenda` (`fkIdEstado`);

--
-- Indices de la tabla `aprendiz`
--
ALTER TABLE `aprendiz`
  ADD PRIMARY KEY (`IdAprendiz`),
  ADD UNIQUE KEY `Correo` (`Correo`),
  ADD UNIQUE KEY `Contraseña` (`Contraseña`),
  ADD KEY `AprendizFicha` (`fkIdFicha`),
  ADD KEY `AprendizTipoDocumento` (`fkIdTipoDocumento`),
  ADD KEY `AprendizEntidad` (`fkIdEntidad`),
  ADD KEY `AprendizCargo` (`fkIdCargo`);

--
-- Indices de la tabla `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`IdArea`);

--
-- Indices de la tabla `asignacionaprendizbitacora`
--
ALTER TABLE `asignacionaprendizbitacora`
  ADD PRIMARY KEY (`IdAsignacionAprendizBitacora`),
  ADD KEY `AsignacionAprendizBitacora` (`fkIdAprendiz`),
  ADD KEY `AsignacionBitacora` (`fkIdBitacora`);

--
-- Indices de la tabla `asignacionaprendizegresado`
--
ALTER TABLE `asignacionaprendizegresado`
  ADD PRIMARY KEY (`IdAsignacionAprendizEgresado`),
  ADD KEY `AsignacionAprendizActividad` (`fkIdActividad`),
  ADD KEY `AsignacionAprendizEgresado` (`fkIdAPrendiz`),
  ADD KEY `AsignacionAprendizEstado` (`fkIdEstado`);

--
-- Indices de la tabla `asignacioncargo`
--
ALTER TABLE `asignacioncargo`
  ADD PRIMARY KEY (`IdAsignacionCargo`),
  ADD KEY `AsignacionCargoPersona` (`fkIdPersona`),
  ADD KEY `AsignacionCargo` (`fkIdCargo`);

--
-- Indices de la tabla `asignacioncompromisos`
--
ALTER TABLE `asignacioncompromisos`
  ADD PRIMARY KEY (`IdAsignacionCompromisos`),
  ADD KEY `AsignacionCompromisosActa` (`fkIdActa`),
  ADD KEY `AsignacionCompromisos` (`fkIdCompromisos`);

--
-- Indices de la tabla `asignacionentidadagenda`
--
ALTER TABLE `asignacionentidadagenda`
  ADD PRIMARY KEY (`IdAsignacionEntidadAgenda`),
  ADD KEY `AsignacionEntidadAgenda` (`fkIdAgenda`),
  ADD KEY `AsignacionEntidad` (`fkIdEntidad`);

--
-- Indices de la tabla `asignacionestadoaprendiz`
--
ALTER TABLE `asignacionestadoaprendiz`
  ADD PRIMARY KEY (`IdAsignacionEstadoAprendiz`),
  ADD KEY `AsignacionEstado` (`fkIdEstado`),
  ADD KEY `AsignacionEstadoAprendiz` (`fkIdAprendiz`);

--
-- Indices de la tabla `asignacionpersonaficha`
--
ALTER TABLE `asignacionpersonaficha`
  ADD PRIMARY KEY (`IdAsignacionPersonaFicha`),
  ADD KEY `AsignacionPersonaFicha` (`fkIdPersona`),
  ADD KEY `AsignacionFicha` (`fkIdFicha`);

--
-- Indices de la tabla `asignacionpersonaparticipanteacta`
--
ALTER TABLE `asignacionpersonaparticipanteacta`
  ADD PRIMARY KEY (`IdAsignacionPersonaParticipanteActa`),
  ADD KEY `AsignacionPersona` (`fkIdPersona`),
  ADD KEY `AsignacionActa` (`fkIdActa`),
  ADD KEY `AsignacionTipoParticipante` (`fkIdTipoParticipante`);

--
-- Indices de la tabla `asignacionrequisitosaprendiz`
--
ALTER TABLE `asignacionrequisitosaprendiz`
  ADD PRIMARY KEY (`IdAsignacionRequisitosAprendiz`),
  ADD KEY `AsignacionAprendiz` (`fkIdAprendiz`),
  ADD KEY `AprendizEstadoRequsitos` (`fkIdEstado`),
  ADD KEY `AsignacionRequisitoCertificacion` (`fkIdRequisitoCertificacion`);

--
-- Indices de la tabla `asignaciontarea`
--
ALTER TABLE `asignaciontarea`
  ADD PRIMARY KEY (`IdAsignacionTarea`),
  ADD KEY `AsignacionTarea` (`fkIdTarea`),
  ADD KEY `AsignacionTareaBitacora` (`fkIdBitacora`);

--
-- Indices de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  ADD PRIMARY KEY (`IdBitacora`),
  ADD KEY `BitacoraCiudad` (`fkIdCiudad`),
  ADD KEY `BitacoraEntidad` (`fkIdEntidad`),
  ADD KEY `BitacoraPersona` (`fkIdPersona`);

--
-- Indices de la tabla `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`IdCargo`);

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`IdCiudad`);

--
-- Indices de la tabla `compromisos`
--
ALTER TABLE `compromisos`
  ADD PRIMARY KEY (`IdCompromisos`),
  ADD KEY `CompromisoPersona` (`fkIdPersona`);

--
-- Indices de la tabla `entidad`
--
ALTER TABLE `entidad`
  ADD PRIMARY KEY (`IdEntidad`),
  ADD KEY `EntidadTipo` (`fkIdTipoEntidad`),
  ADD KEY `EntidadCiudad` (`fkIdCiudad`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`IdEstado`),
  ADD KEY `EstadoTipo` (`fkIdTipoEstado`);

--
-- Indices de la tabla `ficha`
--
ALTER TABLE `ficha`
  ADD PRIMARY KEY (`IdFicha`),
  ADD KEY `FichaPrograma` (`fkIdPrograma`),
  ADD KEY `FichaEstado` (`fkIdEstado`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`IdPersona`),
  ADD UNIQUE KEY `Contraseña` (`Contraseña`),
  ADD KEY `TipoDocumentoPersona` (`fkIdTipoDocumento`),
  ADD KEY `PersonaEntidad` (`fkIdEntidad`),
  ADD KEY `EstadoPersona` (`fkIdEstado`);

--
-- Indices de la tabla `pqr`
--
ALTER TABLE `pqr`
  ADD PRIMARY KEY (`IdPQR`),
  ADD KEY `PQRAprendiz` (`fkIdAprendiz`),
  ADD KEY `PQRTipoPQR` (`fkIdTipoPQR`),
  ADD KEY `AreaPQR` (`fkIdArea`),
  ADD KEY `EstadoPQR` (`fkIdEstado`),
  ADD KEY `PQRPersona` (`fkIdPersona`);

--
-- Indices de la tabla `programa`
--
ALTER TABLE `programa`
  ADD PRIMARY KEY (`IdPrograma`),
  ADD KEY `ProgramaArea` (`fkIdArea`),
  ADD KEY `ProgramaEstado` (`fkIdEstado`);

--
-- Indices de la tabla `requisitocertificacion`
--
ALTER TABLE `requisitocertificacion`
  ADD PRIMARY KEY (`IdRequisitoCertificacion`),
  ADD KEY `RequisitoCertificacion` (`fkIdEstado`);

--
-- Indices de la tabla `tarea`
--
ALTER TABLE `tarea`
  ADD PRIMARY KEY (`IdTarea`);

--
-- Indices de la tabla `tipoactividad`
--
ALTER TABLE `tipoactividad`
  ADD PRIMARY KEY (`IdTipoActividad`);

--
-- Indices de la tabla `tipodocumento`
--
ALTER TABLE `tipodocumento`
  ADD PRIMARY KEY (`IdTipoDocumento`);

--
-- Indices de la tabla `tipoentidad`
--
ALTER TABLE `tipoentidad`
  ADD PRIMARY KEY (`IdTipoEntidad`);

--
-- Indices de la tabla `tipoestado`
--
ALTER TABLE `tipoestado`
  ADD PRIMARY KEY (`IdTipoEstado`);

--
-- Indices de la tabla `tipoparticipante`
--
ALTER TABLE `tipoparticipante`
  ADD PRIMARY KEY (`IdTipoParticipante`);

--
-- Indices de la tabla `tipopqr`
--
ALTER TABLE `tipopqr`
  ADD PRIMARY KEY (`IdTipoPQR`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `acta`
--
ALTER TABLE `acta`
  MODIFY `IdActa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `actividad`
--
ALTER TABLE `actividad`
  MODIFY `IdActividad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `agenda`
--
ALTER TABLE `agenda`
  MODIFY `IdAgenda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `aprendiz`
--
ALTER TABLE `aprendiz`
  MODIFY `IdAprendiz` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT de la tabla `area`
--
ALTER TABLE `area`
  MODIFY `IdArea` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `asignacionaprendizbitacora`
--
ALTER TABLE `asignacionaprendizbitacora`
  MODIFY `IdAsignacionAprendizBitacora` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `asignacionaprendizegresado`
--
ALTER TABLE `asignacionaprendizegresado`
  MODIFY `IdAsignacionAprendizEgresado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `asignacioncargo`
--
ALTER TABLE `asignacioncargo`
  MODIFY `IdAsignacionCargo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `asignacioncompromisos`
--
ALTER TABLE `asignacioncompromisos`
  MODIFY `IdAsignacionCompromisos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `asignacionentidadagenda`
--
ALTER TABLE `asignacionentidadagenda`
  MODIFY `IdAsignacionEntidadAgenda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `asignacionestadoaprendiz`
--
ALTER TABLE `asignacionestadoaprendiz`
  MODIFY `IdAsignacionEstadoAprendiz` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT de la tabla `asignacionpersonaficha`
--
ALTER TABLE `asignacionpersonaficha`
  MODIFY `IdAsignacionPersonaFicha` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `asignacionpersonaparticipanteacta`
--
ALTER TABLE `asignacionpersonaparticipanteacta`
  MODIFY `IdAsignacionPersonaParticipanteActa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `asignacionrequisitosaprendiz`
--
ALTER TABLE `asignacionrequisitosaprendiz`
  MODIFY `IdAsignacionRequisitosAprendiz` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `asignaciontarea`
--
ALTER TABLE `asignaciontarea`
  MODIFY `IdAsignacionTarea` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  MODIFY `IdBitacora` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `cargo`
--
ALTER TABLE `cargo`
  MODIFY `IdCargo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  MODIFY `IdCiudad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `compromisos`
--
ALTER TABLE `compromisos`
  MODIFY `IdCompromisos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `entidad`
--
ALTER TABLE `entidad`
  MODIFY `IdEntidad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `estado`
--
ALTER TABLE `estado`
  MODIFY `IdEstado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT de la tabla `ficha`
--
ALTER TABLE `ficha`
  MODIFY `IdFicha` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `IdPersona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `pqr`
--
ALTER TABLE `pqr`
  MODIFY `IdPQR` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `programa`
--
ALTER TABLE `programa`
  MODIFY `IdPrograma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `requisitocertificacion`
--
ALTER TABLE `requisitocertificacion`
  MODIFY `IdRequisitoCertificacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `tarea`
--
ALTER TABLE `tarea`
  MODIFY `IdTarea` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `tipoactividad`
--
ALTER TABLE `tipoactividad`
  MODIFY `IdTipoActividad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tipodocumento`
--
ALTER TABLE `tipodocumento`
  MODIFY `IdTipoDocumento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `tipoentidad`
--
ALTER TABLE `tipoentidad`
  MODIFY `IdTipoEntidad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tipoestado`
--
ALTER TABLE `tipoestado`
  MODIFY `IdTipoEstado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `tipoparticipante`
--
ALTER TABLE `tipoparticipante`
  MODIFY `IdTipoParticipante` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `tipopqr`
--
ALTER TABLE `tipopqr`
  MODIFY `IdTipoPQR` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `acta`
--
ALTER TABLE `acta`
  ADD CONSTRAINT `ActaEntidad` FOREIGN KEY (`fkIdEntidad`) REFERENCES `entidad` (`IdEntidad`),
  ADD CONSTRAINT `CiudadActa` FOREIGN KEY (`fkIdCiudad`) REFERENCES `ciudad` (`IdCiudad`);

--
-- Filtros para la tabla `actividad`
--
ALTER TABLE `actividad`
  ADD CONSTRAINT `ActividadTipo` FOREIGN KEY (`fkIdTipoActividad`) REFERENCES `tipoactividad` (`IdTipoActividad`);

--
-- Filtros para la tabla `agenda`
--
ALTER TABLE `agenda`
  ADD CONSTRAINT `AgendaPersona` FOREIGN KEY (`fkIdPersona`) REFERENCES `persona` (`IdPersona`),
  ADD CONSTRAINT `EstadoAgenda` FOREIGN KEY (`fkIdEstado`) REFERENCES `estado` (`IdEstado`);

--
-- Filtros para la tabla `aprendiz`
--
ALTER TABLE `aprendiz`
  ADD CONSTRAINT `AprendizCargo` FOREIGN KEY (`fkIdCargo`) REFERENCES `cargo` (`IdCargo`),
  ADD CONSTRAINT `AprendizEntidad` FOREIGN KEY (`fkIdEntidad`) REFERENCES `entidad` (`IdEntidad`),
  ADD CONSTRAINT `AprendizFicha` FOREIGN KEY (`fkIdFicha`) REFERENCES `ficha` (`IdFicha`),
  ADD CONSTRAINT `AprendizTipoDocumento` FOREIGN KEY (`fkIdTipoDocumento`) REFERENCES `tipodocumento` (`IdTipoDocumento`);

--
-- Filtros para la tabla `asignacionaprendizbitacora`
--
ALTER TABLE `asignacionaprendizbitacora`
  ADD CONSTRAINT `AsignacionAprendizBitacora` FOREIGN KEY (`fkIdAprendiz`) REFERENCES `aprendiz` (`IdAprendiz`),
  ADD CONSTRAINT `AsignacionBitacora` FOREIGN KEY (`fkIdBitacora`) REFERENCES `bitacora` (`IdBitacora`);

--
-- Filtros para la tabla `asignacionaprendizegresado`
--
ALTER TABLE `asignacionaprendizegresado`
  ADD CONSTRAINT `AsignacionAprendizActividad` FOREIGN KEY (`fkIdActividad`) REFERENCES `actividad` (`IdActividad`),
  ADD CONSTRAINT `AsignacionAprendizEgresado` FOREIGN KEY (`fkIdAPrendiz`) REFERENCES `aprendiz` (`IdAprendiz`),
  ADD CONSTRAINT `AsignacionAprendizEstado` FOREIGN KEY (`fkIdEstado`) REFERENCES `estado` (`IdEstado`);

--
-- Filtros para la tabla `asignacioncargo`
--
ALTER TABLE `asignacioncargo`
  ADD CONSTRAINT `AsignacionCargo` FOREIGN KEY (`fkIdCargo`) REFERENCES `cargo` (`IdCargo`),
  ADD CONSTRAINT `AsignacionCargoPersona` FOREIGN KEY (`fkIdPersona`) REFERENCES `persona` (`IdPersona`);

--
-- Filtros para la tabla `asignacioncompromisos`
--
ALTER TABLE `asignacioncompromisos`
  ADD CONSTRAINT `AsignacionCompromisos` FOREIGN KEY (`fkIdCompromisos`) REFERENCES `compromisos` (`IdCompromisos`),
  ADD CONSTRAINT `AsignacionCompromisosActa` FOREIGN KEY (`fkIdActa`) REFERENCES `acta` (`IdActa`);

--
-- Filtros para la tabla `asignacionentidadagenda`
--
ALTER TABLE `asignacionentidadagenda`
  ADD CONSTRAINT `AsignacionEntidad` FOREIGN KEY (`fkIdEntidad`) REFERENCES `entidad` (`IdEntidad`),
  ADD CONSTRAINT `AsignacionEntidadAgenda` FOREIGN KEY (`fkIdAgenda`) REFERENCES `agenda` (`IdAgenda`);

--
-- Filtros para la tabla `asignacionestadoaprendiz`
--
ALTER TABLE `asignacionestadoaprendiz`
  ADD CONSTRAINT `AsignacionEstado` FOREIGN KEY (`fkIdEstado`) REFERENCES `estado` (`IdEstado`),
  ADD CONSTRAINT `AsignacionEstadoAprendiz` FOREIGN KEY (`fkIdAprendiz`) REFERENCES `aprendiz` (`IdAprendiz`);

--
-- Filtros para la tabla `asignacionpersonaficha`
--
ALTER TABLE `asignacionpersonaficha`
  ADD CONSTRAINT `AsignacionFicha` FOREIGN KEY (`fkIdFicha`) REFERENCES `ficha` (`IdFicha`),
  ADD CONSTRAINT `AsignacionPersonaFicha` FOREIGN KEY (`fkIdPersona`) REFERENCES `persona` (`IdPersona`);

--
-- Filtros para la tabla `asignacionpersonaparticipanteacta`
--
ALTER TABLE `asignacionpersonaparticipanteacta`
  ADD CONSTRAINT `AsignacionActa` FOREIGN KEY (`fkIdActa`) REFERENCES `acta` (`IdActa`),
  ADD CONSTRAINT `AsignacionPersona` FOREIGN KEY (`fkIdPersona`) REFERENCES `persona` (`IdPersona`),
  ADD CONSTRAINT `AsignacionTipoParticipante` FOREIGN KEY (`fkIdTipoParticipante`) REFERENCES `tipoparticipante` (`IdTipoParticipante`);

--
-- Filtros para la tabla `asignacionrequisitosaprendiz`
--
ALTER TABLE `asignacionrequisitosaprendiz`
  ADD CONSTRAINT `AprendizEstadoRequsitos` FOREIGN KEY (`fkIdEstado`) REFERENCES `estado` (`IdEstado`),
  ADD CONSTRAINT `AsignacionAprendiz` FOREIGN KEY (`fkIdAprendiz`) REFERENCES `aprendiz` (`IdAprendiz`),
  ADD CONSTRAINT `AsignacionRequisitoCertificacion` FOREIGN KEY (`fkIdRequisitoCertificacion`) REFERENCES `requisitocertificacion` (`IdRequisitoCertificacion`);

--
-- Filtros para la tabla `asignaciontarea`
--
ALTER TABLE `asignaciontarea`
  ADD CONSTRAINT `AsignacionTarea` FOREIGN KEY (`fkIdTarea`) REFERENCES `tarea` (`IdTarea`),
  ADD CONSTRAINT `AsignacionTareaBitacora` FOREIGN KEY (`fkIdBitacora`) REFERENCES `bitacora` (`IdBitacora`);

--
-- Filtros para la tabla `bitacora`
--
ALTER TABLE `bitacora`
  ADD CONSTRAINT `BitacoraCiudad` FOREIGN KEY (`fkIdCiudad`) REFERENCES `ciudad` (`IdCiudad`),
  ADD CONSTRAINT `BitacoraEntidad` FOREIGN KEY (`fkIdEntidad`) REFERENCES `entidad` (`IdEntidad`),
  ADD CONSTRAINT `BitacoraPersona` FOREIGN KEY (`fkIdPersona`) REFERENCES `persona` (`IdPersona`);

--
-- Filtros para la tabla `compromisos`
--
ALTER TABLE `compromisos`
  ADD CONSTRAINT `CompromisoPersona` FOREIGN KEY (`fkIdPersona`) REFERENCES `persona` (`IdPersona`);

--
-- Filtros para la tabla `entidad`
--
ALTER TABLE `entidad`
  ADD CONSTRAINT `EntidadCiudad` FOREIGN KEY (`fkIdCiudad`) REFERENCES `ciudad` (`IdCiudad`),
  ADD CONSTRAINT `EntidadTipo` FOREIGN KEY (`fkIdTipoEntidad`) REFERENCES `tipoentidad` (`IdTipoEntidad`);

--
-- Filtros para la tabla `estado`
--
ALTER TABLE `estado`
  ADD CONSTRAINT `EstadoTipo` FOREIGN KEY (`fkIdTipoEstado`) REFERENCES `tipoestado` (`IdTipoEstado`);

--
-- Filtros para la tabla `ficha`
--
ALTER TABLE `ficha`
  ADD CONSTRAINT `FichaEstado` FOREIGN KEY (`fkIdEstado`) REFERENCES `estado` (`IdEstado`),
  ADD CONSTRAINT `FichaPrograma` FOREIGN KEY (`fkIdPrograma`) REFERENCES `programa` (`IdPrograma`);

--
-- Filtros para la tabla `persona`
--
ALTER TABLE `persona`
  ADD CONSTRAINT `EstadoPersona` FOREIGN KEY (`fkIdEstado`) REFERENCES `estado` (`IdEstado`),
  ADD CONSTRAINT `PersonaEntidad` FOREIGN KEY (`fkIdEntidad`) REFERENCES `entidad` (`IdEntidad`),
  ADD CONSTRAINT `TipoDocumentoPersona` FOREIGN KEY (`fkIdTipoDocumento`) REFERENCES `tipodocumento` (`IdTipoDocumento`);

--
-- Filtros para la tabla `pqr`
--
ALTER TABLE `pqr`
  ADD CONSTRAINT `AreaPQR` FOREIGN KEY (`fkIdArea`) REFERENCES `area` (`IdArea`),
  ADD CONSTRAINT `EstadoPQR` FOREIGN KEY (`fkIdEstado`) REFERENCES `estado` (`IdEstado`),
  ADD CONSTRAINT `PQRAprendiz` FOREIGN KEY (`fkIdAprendiz`) REFERENCES `aprendiz` (`IdAprendiz`),
  ADD CONSTRAINT `PQRPersona` FOREIGN KEY (`fkIdPersona`) REFERENCES `persona` (`IdPersona`),
  ADD CONSTRAINT `PQRTipoPQR` FOREIGN KEY (`fkIdTipoPQR`) REFERENCES `tipopqr` (`IdTipoPQR`);

--
-- Filtros para la tabla `programa`
--
ALTER TABLE `programa`
  ADD CONSTRAINT `ProgramaArea` FOREIGN KEY (`fkIdArea`) REFERENCES `area` (`IdArea`),
  ADD CONSTRAINT `ProgramaEstado` FOREIGN KEY (`fkIdEstado`) REFERENCES `estado` (`IdEstado`);

--
-- Filtros para la tabla `requisitocertificacion`
--
ALTER TABLE `requisitocertificacion`
  ADD CONSTRAINT `RequisitoCertificacion` FOREIGN KEY (`fkIdEstado`) REFERENCES `estado` (`IdEstado`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
