Create database BDArticulacion
use BDArticulacion


Create table Area(
IdArea int primary key auto_increment,
NombreArea varchar (90)
);



Create table Cargo(
IdCargo int primary key auto_increment,
NombreCargo varchar(90)
);




Create table Tarea(
IdTarea int primary key auto_increment,
Observacion varchar (90),
Actividad varchar(90),
Fecha date
);



Create table TipoDocumento(
IdTipoDocumento int primary key auto_increment,
NombreTipoDocumento varchar(90)
);


Create table TipoEntidad(
IdTipoEntidad int primary key auto_increment,
NombreTipoEntidad varchar(90)
);


Create table TipoParticipante(
IdTipoParticipante int primary key auto_increment,
NombreTipoParticipante varchar(90)
);


create table TipoPQR(
IdTipoPQR int primary key auto_increment,
Nombre varchar(90)
);

create table Ciudad(
IdCiudad int primary key auto_increment,
NombreCiudad varchar(90)
);


Create table TipoEstado(
IdTipoEstado int primary key auto_increment,
NombreTipoEstado varchar(90)
);


Create table Estado(
IdEstado int primary key auto_increment,
NombreEstado varchar(90),
fkIdTipoEstado int not null,
foreign key EstadoTipo(fkIdTipoEstado) references TipoEstado(IdTipoEstado)
);






create table Entidad(
IdEntidad int primary key auto_increment,
fkIdTipoEntidad int not null,
Nit int,
Nombre varchar(90),
Telefono varchar(90),
Direccion varchar(90),
fkIdCiudad int not null,
foreign key EntidadTipo(fkIdTipoEntidad) references TipoEntidad(IdTipoEntidad),
foreign key EntidadCiudad(fkIdCiudad) references Ciudad(IdCiudad)
);

Create table Persona(
IdPersona int primary key auto_increment,
fkIdTipoDocumento int not null,
Documento varchar(50) not null,
Nombre varchar(90),
Apellido varchar(90),
FechaNacimiento varchar(50),
Telefono bigint(50),
Correo varchar(90) unique,
Contraseña varchar(30)unique,
fkIdEntidad int not null,
fkIdEstado int not null,
foreign key TipoDocumentoPersona(fkIdTipoDocumento) references TipoDocumento(IdTipoDocumento),
foreign key PersonaEntidad(fkIdEntidad) references Entidad(IdEntidad),
foreign key EstadoPersona(fkIdEstado) references Estado(IdEstado)
);



create table AsignacionCargo(
IdAsignacionCargo int primary key auto_increment,
fkIdPersona int not null,
foreign key AsignacionCargoPersona(fkIdPersona) references persona(IdPersona),
fkIdCargo int not null,
foreign key AsignacionCargo(fkIdCargo) references cargo(Idcargo)
);



create table Agenda(
IdAgenda int primary key auto_increment,
FechaProgramada date,
fkIdPersona int not null,
fkIdEstado int not null,
foreign key AgendaPersona(fkIdPersona) references Persona(IdPersona),
foreign key EstadoAgenda(fkIdEstado) references Estado(IdEstado)
);


create table AsignacionEntidadAgenda(
IdAsignacionEntidadAgenda int primary key auto_increment,
fkIdAgenda int not null,
fkIdEntidad int not null,
foreign key AsignacionEntidadAgenda(fkIdAgenda) references Agenda(IdAgenda),
foreign key AsignacionEntidad(fkIdEntidad) references Entidad(IdEntidad)
);



create table Acta(
IdActa int primary key auto_increment,
Fecha timestamp,
fkIdCiudad int not null,
HoraInicio datetime,
HoraFinalizacion datetime,
fkIdEntidad int not null,
Temas varchar(100),
Objetivo varchar(100),
Desarrollo varchar(100),
Conclusiones varchar(100),
ArchivoAdjuntado varchar(100),
foreign key ActaEntidad(fkIdEntidad) references Entidad(IdEntidad),
foreign key CiudadActa(fkIdCiudad) references Ciudad(IdCiudad)
);



create table AsignacionPersonaParticipanteActa(
IdAsignacionPersonaParticipanteActa int primary key auto_increment,
fkIdTipoParticipante int not null,
fkIdPersona int not null,
fkIdActa int not null,
Firma  varchar(100),
foreign key AsignacionPersona(fkIdPersona) references Persona(IdPersona),
foreign key AsignacionActa(fkIdActa) references Acta(IdActa),
foreign key AsignacionTipoParticipante(fkIdTipoParticipante) references TipoParticipante(IdTipoParticipante)
);



create table Compromisos(
IdCompromisos int primary key auto_increment,
Actividad varchar(100),
fkIdPersona int not null,
Fecha date,
foreign key CompromisoPersona(fkIdPersona) references Persona(IdPersona)
);


create table AsignacionCompromisos(
IdAsignacionCompromisos int primary key auto_increment,
fkIdActa int not null,
fkIdCompromisos int not null,
foreign key AsignacionCompromisosActa(fkIdActa) references Acta(IdActa),
foreign key AsignacionCompromisos(fkIdCompromisos) references Compromisos(IdCompromisos)
);



create table Programa(
IdPrograma int primary key auto_increment,
Nombre varchar(90),
fkIdArea int not null,
foreign key ProgramaArea(fkIdArea) references Area(IdArea),
fkIdEstado int not null,
foreign key ProgramaEstado(fkIdEstado) references Estado(IdEstado)
);


create table Ficha(
IdFicha int primary key auto_increment,
NumeroFicha varchar(99),
fkIdPrograma int not null,
foreign key FichaPrograma(fkIdPrograma) references Programa(IdPrograma),
fkIdEstado int not null,
foreign key FichaEstado(fkIdEstado) references Estado(IdEstado)
);



create table AsignacionPersonaFicha(
IdAsignacionPersonaFicha int primary key auto_increment,
fkIdPersona int not null,
foreign key AsignacionPersonaFicha(fkIdPersona) references Persona(IdPersona),
fkIdFicha int not null,
foreign key AsignacionFicha(fkIdFicha) references Ficha(IdFicha)
);


create table RequisitoCertificacion(
IdRequisitoCertificacion int primary key auto_increment,
Requisito varchar(100),
fkIdEstado int not null,
foreign key RequisitoCertificacion(fkIdEstado) references Estado(IdEstado)
);


create table Aprendiz(
IdAprendiz int primary key auto_increment,
fkIdTipoDocumento int not null,
Documento varchar(90),
Nombre varchar(100),
Apellido varchar(100),
FechaNacimiento date,
Telefono varchar(90),
Correo varchar(90) UNIQUE,
Contraseña varchar(89) unique,
fkIdFicha int not null,
fkIdEntidad int not null,
fkIdCargo int not null,
foreign key AprendizFicha(fkIdFicha) references Ficha(IdFicha),
foreign key AprendizTipoDocumento(fkIdTipoDocumento) references TipoDocumento(IdTipoDocumento),
foreign key AprendizEntidad(fkIdEntidad) references Entidad(IdEntidad),
foreign key AprendizCargo(fkIdCargo) references Cargo(IdCargo)
);	



create table AsignacionRequisitosAprendiz(
IdAsignacionRequisitosAprendiz int primary key auto_increment,
fkIdAprendiz int not null,
fkIdRequisitoCertificacion int not null,
fkIdEstado int not null,
FOREIGN KEY AsignacionAprendiz(fkIdAprendiz) REFERENCES Aprendiz (IdAprendiz),
Foreign key AprendizEstadoRequsitos(fkIdEstado) references Estado(IdEstado),
FOREIGN KEY AsignacionRequisitoCertificacion(fkIdRequisitoCertificacion) REFERENCES RequisitoCertificacion (IdRequisitoCertificacion)
);

	

create table Bitacora(
IdBitacora int primary key auto_increment,
Fecha timestamp,
fkIdCiudad int not null,
fkIdEntidad int not null,
fkIdPersona int not null,
Firma varchar(30),
ArchivoAdjunto varchar(50),
foreign key BitacoraCiudad(fkIdCiudad) references Ciudad(IdCiudad),
foreign key BitacoraEntidad(fkIdEntidad) references Entidad(IdEntidad),
foreign key BitacoraPersona(fkIdPersona) references Persona(IdPersona)
);


create table AsignacionTarea(
IdAsignacionTarea int primary key auto_increment,
fkIdTarea int not null,
fkIdBitacora int not null,
FOREIGN KEY AsignacionTarea(fkIdTarea) REFERENCES tarea (IdTarea),
FOREIGN KEY AsignacionTareaBitacora(fkIdBitacora) REFERENCES Bitacora (IdBitacora)    
);



create table AsignacionAprendizBitacora(
IdAsignacionAprendizBitacora int primary key auto_increment,
fkIdBitacora int not null,
fkIdAprendiz int not null,
FOREIGN KEY AsignacionAprendizBitacora(fkIdAprendiz) REFERENCES Aprendiz(IdAprendiz),
FOREIGN KEY AsignacionBitacora(fkIdBitacora) REFERENCES Bitacora (IdBitacora)
);


create table AsignacionEstadoAprendiz(
IdAsignacionEstadoAprendiz int primary key auto_increment,
fkIdEstado int not null,
fkIdAprendiz int not null,
FOREIGN KEY AsignacionEstado(fkIdEstado) references estado(IdEstado),
FOREIGN KEY AsignacionEstadoAprendiz(fkIdAprendiz) references Aprendiz(IdAprendiz)
);




create table PQR(
IdPQR int primary key auto_increment,
Fecha date,
fkIdAprendiz int not null,
fkIdTipoPQR int not null,
Asunto varchar(90),
fkIdArea int not null,
Descripcion varchar(90),
fkIdEstado int not null,
fkIdPersona int null,
Respuesta varchar(90),
FOREIGN KEY PQRAprendiz(fkIdAprendiz) references Aprendiz(IdAprendiz),
FOREIGN KEY PQRTipoPQR(fkidTipoPQR ) references TipoPQR(idTipoPQR),
FOREIGN KEY AreaPQR(fkIdArea ) references Area(IdArea),
FOREIGN KEY EstadoPQR(fkIdEstado ) references Estado(IdEstado),
FOREIGN KEY PQRPersona(fkIdPersona ) references Persona(IdPersona)
);

Create table TipoActividad(
IdTipoActividad int primary key auto_increment,
NombreTipo varchar(30)
);


create table Actividad(
IdActividad int primary key auto_increment,
fkIdTipoActividad int not null,
Descripción varchar(90),
CargoCarrera varchar(90),
Entidad varchar(90),
TiempoTranscurrido timestamp,
Foreign key ActividadTipo(fkIdTipoActividad) references TipoActividad(IdTipoActividad)
);




create table AsignacionAprendizEgresado(
IdAsignacionAprendizEgresado int primary key auto_increment,
fkIdAPrendiz int not null,
fkIdActividad int not null,
fkIdEstado int not null,
FOREIGN KEY AsignacionAprendizActividad(fkIdActividad) references Actividad (IdActividad),
FOREIGN KEY AsignacionAprendizEgresado(fkIdAPrendiz ) references APrendiz (IdAPrendiz),
FOREIGN KEY AsignacionAprendizEstado(fkIdEstado ) references Estado (IdEstado)
);



